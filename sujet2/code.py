def log_content(func):
    ch =" hi there"
    def sum(a, b , c):
        print("variable a = " , a)
        print("variable b = " , b)
        print("variable c = " , c)
        print("variable d = " , ch )
        ret = a +b +c
        print('variable result = {}'.format(ret))
        return ret
    return sum
        
@log_content
def example(a, b, c):
    a = b + c
    b = a + c
    c = a + b
    d = "hi there"
    result = a + b + c
    return result


print(example(5 , 4 ,3))